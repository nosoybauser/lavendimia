import { Component, OnInit } from '@angular/core';
import { VentaService } from '../services/venta.service';
import { Articulo } from '../model/articulo';
import { Venta } from '../model/venta';

@Component({
  selector: 'app-venta',
  templateUrl: './venta.component.html',
  styleUrls: ['./venta.component.css']
})
export class VentaComponent implements OnInit {
  ventas: Venta[];
  constructor(private ventaService: VentaService) { }

  ngOnInit() {
    this.getVentas();
  }
  getVentas(): void {
    this.ventaService.getVentas()
    .subscribe(ventas => this.ventas = ventas);
  }

}
