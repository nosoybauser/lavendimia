import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Cliente } from '../model/cliente';
import { catchError, map } from 'rxjs/operators';
import { Observable, of } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ClienteService {
  clienteUrl = environment.apiUrl + '/cliente';
  httpOptions = {
    headers: new HttpHeaders({'Content-type': 'application/json'})
  };
  constructor(private http: HttpClient) { }

  // consume get
  getClientes(): Observable<Cliente[]> {
    return this.http.get<Cliente[]>(this.clienteUrl)
    .pipe(
      catchError(this.handleError<Cliente[]>('getCliente', []))
    );
  }
   /** GET articulo by id. Return `undefined` when id not found */
   getClienteNo404<Data>(id: number): Observable<Cliente> {
    const url = `${this.clienteUrl}/?id=${id}`;
    return this.http.get<Cliente[]>(url)
      .pipe(
        map(articulos => articulos[0]), // returns a {0|1} element array
        catchError(this.handleError<Cliente>(`getArticulo id=${id}`))
      );
  }
  // consume get by id
  getCliente(id: number): Observable<Cliente> {
    const url = `${this.clienteUrl}/${id}`;
    return this.http.get<Cliente>(url)
    .pipe(
      catchError(this.handleError<Cliente>(`getCliente id = ${id}`))
    );
   }
   // consume put
   updateCliente(cliente: Cliente): Observable<any> {
     return this.http.put(`${this.clienteUrl}/${cliente.id}`, cliente, this.httpOptions)
     .pipe(
       catchError(this.handleError<any>('updateCliente'))
     );
   }
   // consume post
   addCliente(cliente: Cliente): Observable<Cliente> {
    return this.http.post<Cliente>(this.clienteUrl, cliente, this.httpOptions)
    .pipe(
      catchError(this.handleError<Cliente>('addCliente'))
    );
  }
  // consume delete
  deleteCliente(cliente: Cliente | number): Observable<Cliente> {
    const id  = typeof cliente === 'number' ? cliente : cliente.nombre;
    const url = `${this.clienteUrl}/${id}`;

    return this.http.delete<Cliente>(url, this.httpOptions)
    .pipe(
      catchError(this.handleError<Cliente>('deleteCliente'))
    );
  }
   // validaciones...
private handleError<T>(operation = 'operation', result?: T) {
  return (error: any): Observable<T> => {

    // TODO: send the error to remote logging infrastructure
    console.error(error); // log to console instead

    // Let the app keep running by returning an empty result.
    return of(result as T);
  };
}
}
