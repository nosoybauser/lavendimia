import { Injectable } from '@angular/core';
import { Venta } from '../model/venta';
import { Observable, of } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class VentaService {
  ventaUrl = environment.apiUrl + '/venta';
  httpOptions = {
    headers: new HttpHeaders({'Content-type': 'application/json'})
  };
  constructor(private http: HttpClient) { }
  // consume get
  getVentas(): Observable<Venta[]> {
    return this.http.get<Venta[]>(this.ventaUrl)
    .pipe(
      catchError(this.handleError<Venta[]>('getVenta', []))
    );
  }
   // consume post
   addVenta(venta: Venta): Observable<Venta> {
    return this.http.post<Venta>(this.ventaUrl, venta, this.httpOptions)
    .pipe(
      catchError(this.handleError<Venta>('addVenta'))
    );
  }
   // validaciones...
private handleError<T>(operation = 'operation', result?: T) {
  return (error: any): Observable<T> => {

    // TODO: send the error to remote logging infrastructure
    console.error(error); // log to console instead

    // Let the app keep running by returning an empty result.
    return of(result as T);
  };
}
}
