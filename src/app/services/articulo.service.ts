import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Observable, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';
import { Articulo } from '../model/articulo';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ArticuloService {
  articuloUrl = environment.apiUrl + '/articulo';
  httpOptions = {
    headers: new HttpHeaders({'Content-type': 'application/json'})
  };
  constructor(private http: HttpClient) { }

  // consume get
  getArticulos(): Observable<Articulo[]> {
    return this.http.get<Articulo[]>(this.articuloUrl)
    .pipe(
      catchError(this.handleError<Articulo[]>('getArticulo', []))
    );
  }
   /** GET articulo by id. Return `undefined` when id not found */
   geNo40tCliente404<Data>(id: number): Observable<Articulo> {
    const url = `${this.articuloUrl}/?id=${id}`;
    return this.http.get<Articulo[]>(url)
      .pipe(
        map(articulos => articulos[0]), // returns a {0|1} element array
        catchError(this.handleError<Articulo>(`getArticulo id=${id}`))
      );
  }
  // consume get by id
  getArticulo(id: number): Observable<Articulo> {
    const url = `${this.articuloUrl}/${id}`;
    return this.http.get<Articulo>(url)
    .pipe(
      catchError(this.handleError<Articulo>(`getArticulo id = ${id}`))
    );
   }
   // consume put
   updateArticulo(articulo: Articulo): Observable<any> {
     return this.http.put(`${this.articuloUrl}/${articulo.id}`, articulo, this.httpOptions)
     .pipe(
       catchError(this.handleError<any>('updateArticulo'))
     );
   }
   // consume post
   addArticulo(articulo: Articulo): Observable<Articulo> {
    return this.http.post<Articulo>(this.articuloUrl, articulo, this.httpOptions)
    .pipe(
      catchError(this.handleError<Articulo>('addArticulo'))
    );
  }
  // consume delete
  deleteArticulo(articulo: Articulo | number): Observable<Articulo> {
    const id  = typeof articulo === 'number' ? articulo : articulo.descripcion;
    const url = `${this.articuloUrl}/${id}`;

    return this.http.delete<Articulo>(url, this.httpOptions)
    .pipe(
      catchError(this.handleError<Articulo>('deleteArticulo'))
    );
  }
   // validaciones...
private handleError<T>(operation = 'operation', result?: T) {
  return (error: any): Observable<T> => {

    // TODO: send the error to remote logging infrastructure
    console.error(error); // log to console instead

    // Let the app keep running by returning an empty result.
    return of(result as T);
  };
}
}
