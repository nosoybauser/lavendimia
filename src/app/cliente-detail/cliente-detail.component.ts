import { Component, OnInit } from '@angular/core';
import { Cliente } from '../model/cliente';
import { ClienteService } from '../services/cliente.service';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-cliente-detail',
  templateUrl: './cliente-detail.component.html',
  styleUrls: ['./cliente-detail.component.css']
})
export class ClienteDetailComponent implements OnInit {
  cliente: Cliente;
  constructor(private clienteService: ClienteService, private route: ActivatedRoute, private location: Location) { }
  clienteForm = new FormGroup({
    id: new FormControl('', Validators.required),
    nombre: new FormControl('', Validators.required),
    ape_pat: new FormControl('', Validators.required),
    ape_mat: new FormControl('', Validators.required),
    rfc: new FormControl('', [Validators.required, Validators.minLength(12), Validators.maxLength(13)])
  });

  ngOnInit() {
    this.getCliente();
  }
  getCliente(): void {
    const id = +this.route.snapshot.paramMap.get('id');
    this.clienteService.getCliente(id)
      .subscribe(cliente => this.cliente = cliente);
  }
  goBack(): void {
    this.location.back();
  }

  save(): void {
    if (!this.clienteForm.valid) {
      console.warn('ups');
    } else {
      console.warn(this.clienteForm.value);
      this.cliente = this.clienteForm.value;
      console.warn(this.cliente);

      this.clienteService.updateCliente(this.cliente)
      .subscribe(() => this.goBack());
    }
  }
}
