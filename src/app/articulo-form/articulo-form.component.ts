import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { Articulo } from '../model/articulo';
import { ArticuloService } from '../services/articulo.service';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
@Component({
  selector: 'app-articulo-form',
  templateUrl: './articulo-form.component.html',
  styleUrls: ['./articulo-form.component.css']
})
export class ArticuloFormComponent implements OnInit {
  constructor(private articuloService: ArticuloService, private route: ActivatedRoute, private location: Location) { }
  articulo: Articulo[];

  articuloForm = new FormGroup({
    descripcion: new FormControl('', Validators.required),
    modelo: new FormControl('', Validators.required),
    precio: new FormControl('', Validators.required),
    existencia: new FormControl('', Validators.required)
  });

  ngOnInit() {
  }

  onSubmit() {
    if (!this.articuloForm.valid) {
      console.warn('ups :P');
    } else {
      console.warn(this.articuloForm.value);
      this.articulo = this.articuloForm.value;
      console.warn(this.articulo);
      this.articuloService.addArticulo(this.articuloForm.value)
       .subscribe(articulo => this.articulo.push(articulo));
      this.goBack();
    }
  }
  goBack(): void {
    this.location.back();
  }
}
