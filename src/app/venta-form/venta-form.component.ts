import { Component, OnInit } from '@angular/core';
import { ClienteService } from '../services/cliente.service';
import { ArticuloService } from '../services/articulo.service';
import { VentaService } from '../services/venta.service';
import { Cliente } from '../model/cliente';
import { FormGroup, FormControl, FormArray } from '@angular/forms';
import { Articulo } from '../model/articulo';

@Component({
  selector: 'app-venta-form',
  templateUrl: './venta-form.component.html',
  styleUrls: ['./venta-form.component.css']
})
export class VentaFormComponent implements OnInit {
  clientes: Cliente[];
  articulos: Articulo[];
  arrayArticulos: Articulo[];

  formClientes = new FormGroup({
    cliente: new FormControl(this.clientes),
  });

  formArticulos = new FormGroup({
    articulo: new FormControl(this.articulos),
  });

  constructor(
    private clienteService: ClienteService,
    private articuloService: ArticuloService,
    private ventaService: VentaService) { }

  ngOnInit() {
    this.getClientes();
    this.getArticulos();
  }
  getClientes(): void {
    this.clienteService.getClientes()
    .subscribe(clientes => this.clientes = clientes);
  }
  getArticulos(): void {
    this.articuloService.getArticulos()
    .subscribe(articulos => this.articulos = articulos);
  }
  addToArray() {
    this.arrayArticulos.push(this.formArticulos.value as Articulo);
  }

}
