import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ClienteComponent } from './cliente/cliente.component';
import { ArticuloComponent } from './articulo/articulo.component';
import { ClienteFormComponent } from './cliente-form/cliente-form.component';
import { ArticuloFormComponent } from './articulo-form/articulo-form.component';
import { ArticuloDetailComponent } from './articulo-detail/articulo-detail.component';
import { ClienteDetailComponent } from './cliente-detail/cliente-detail.component';
import { ArticuloService } from './services/articulo.service';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { VentaFormComponent } from './venta-form/venta-form.component';
import { VentaComponent } from './venta/venta.component';

@NgModule({
  declarations: [
    AppComponent,
    ClienteComponent,
    ArticuloComponent,
    ClienteFormComponent,
    ArticuloFormComponent,
    ArticuloDetailComponent,
    ClienteDetailComponent,
    VentaFormComponent,
    VentaComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [
    ArticuloService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
