import { Component, OnInit } from '@angular/core';
import { Articulo } from '../model/articulo';
import { ArticuloService } from '../services/articulo.service';

@Component({
  selector: 'app-articulo',
  templateUrl: './articulo.component.html',
  styleUrls: ['./articulo.component.css']
})
export class ArticuloComponent implements OnInit {
  articulos: Articulo[];
  constructor(private articuloService: ArticuloService) { }

  ngOnInit() {
    this.getArticulos();
  }

  getArticulos(): void {
    this.articuloService.getArticulos()
    .subscribe(articulos => this.articulos = articulos);
  }
}
