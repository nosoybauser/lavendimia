import { Component, OnInit } from '@angular/core';
import { Articulo } from '../model/articulo';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { ArticuloService } from '../services/articulo.service';

@Component({
  selector: 'app-articulo-detail',
  templateUrl: './articulo-detail.component.html',
  styleUrls: ['./articulo-detail.component.css']
})
export class ArticuloDetailComponent implements OnInit {
  articulo: Articulo;
  constructor(private articuloService: ArticuloService, private route: ActivatedRoute, private location: Location) { }
  articuloForm = new FormGroup({
    id: new FormControl('', Validators.required),
    descripcion: new FormControl('', Validators.required),
    modelo: new FormControl('', Validators.required),
    precio: new FormControl('', Validators.required),
    existencia: new FormControl('', Validators.required)
  });
  ngOnInit() {
    this.getArticulo();
  }
  getArticulo(): void {
    const id = +this.route.snapshot.paramMap.get('id');
    this.articuloService.getArticulo(id)
    .subscribe(articulo => this.articulo = articulo);
  }

  goBack(): void {
    this.location.back();
  }

  save(): void {
    if (!this.articuloForm.valid) {
      console.warn('ups :P');
    } else {
      console.warn(this.articuloForm.value);
      this.articulo = this.articuloForm.value;
      console.warn(this.articulo);

      this.articuloService.updateArticulo(this.articulo)
      .subscribe(() => this.goBack());
    }
  }
}
