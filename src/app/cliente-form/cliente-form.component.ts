import { Component, OnInit } from '@angular/core';
import { Cliente } from '../model/cliente';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ClienteService } from '../services/cliente.service';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';

@Component({
  selector: 'app-cliente-form',
  templateUrl: './cliente-form.component.html',
  styleUrls: ['./cliente-form.component.css']
})
export class ClienteFormComponent implements OnInit {
  cliente: Cliente[];
  constructor(private clienteService: ClienteService, private route: ActivatedRoute, private location: Location) { }
  clienteForm = new FormGroup({
    nombre: new FormControl('', Validators.required),
    ape_pat: new FormControl('', Validators.required),
    ape_mat: new FormControl('', Validators.required),
    rfc: new FormControl('', [Validators.required, Validators.minLength(12), Validators.maxLength(13)])
  });
  ngOnInit() {
  }
  onSubmit() {
    if (this.clienteForm.valid) {
      console.warn(this.clienteForm.value);
      this.cliente = this.clienteForm.value;
      console.warn(this.clienteForm.value);
      this.clienteService.addCliente(this.clienteForm.value)
      .subscribe(cliente => this.cliente.push(cliente));
      this.goBack();
    }
  }
  goBack(): void {
    this.location.back();
  }

}
