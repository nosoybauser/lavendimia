export class Cliente {
    id: number;
    nombre: string;
    apePat: string;
    apeMat: string;
    RFC: string;
  }
