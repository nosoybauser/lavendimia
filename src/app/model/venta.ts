export class Venta {
  id: number;
  cliente_id: number;
  cliente_nombre: string;
  total: number;
  fecha: Date;
}
