import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ArticuloComponent } from './articulo/articulo.component';
import { ClienteComponent } from './cliente/cliente.component';
import { ClienteFormComponent } from './cliente-form/cliente-form.component';
import { ClienteDetailComponent } from './cliente-detail/cliente-detail.component';
import { ArticuloFormComponent } from './articulo-form/articulo-form.component';
import { ArticuloDetailComponent } from './articulo-detail/articulo-detail.component';
import { VentaComponent } from './venta/venta.component';
import { VentaFormComponent } from './venta-form/venta-form.component';

const routes: Routes = [
  { path: '', redirectTo: '/venta', pathMatch: 'full' },
  { path: 'articulo', component: ArticuloComponent },
  { path: 'cliente', component: ClienteComponent },
  { path: 'cliente-form', component: ClienteFormComponent },
  { path: 'cliente-detail/:id', component: ClienteDetailComponent },
  { path: 'articulo-detail/:id', component: ArticuloDetailComponent },
  { path: 'articulo-form', component: ArticuloFormComponent },
  { path: 'venta', component: VentaComponent },
  { path: 'venta-form', component: VentaFormComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
